# DeadLineMiniMonitor
> Version 1.3  


The DeadLine Minimonitor permise to Artist to disable/enable the slave on there machines.  
## Prerequisites : 
`rez-env deadline_minimon` to get all needed librairies :
- PyQt5
    - Use Qt Binding
- Python 3.6
    - Python 2.7 Compatible

Installing :   
look at : http://wiki.circus.dom/NewDokuWiki/doku.php?id=users:tools:deadline_minimon

## Contribuiting :
Arnaud Pisani  
Adrien Meynard  
Olivier Guilquin  

## Versionning :
- Implemented
    - Tray Icon
    - Enable/Disable Slave
    - Double-Click on Tray to show GUI
    - Left Click Menu on Tray
    - Dynamic Status
    - Dynamic TrayIcon
    - Dynamic Slider Position
    - Centos 7 / Windows Support

## Authors :
François Lavigne Marbach  
francois@circus.fr  

## Maintainers :
François Lavigne Marbach

## License : 
All right reserved to circus - 2019  
Do not copy or redistribuate Without permissions

## resources method to create resource file .py from .qrc
C:\Python27\Lib\site-packages\PyQt4\pyrcc4.exe -o resources.py resources.qrc