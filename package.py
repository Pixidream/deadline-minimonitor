requires = ["Qt.py", "deadline", "psutil", "sentry_sdk"]


def commands():
    import os

    execfile(os.path.join(os.path.dirname(os.environ["REZ_CONFIG_FILE"]), "package_common_scripts.py"))
    temp_bin_directory = copy_bin_to_tmp(system, root)

    env.PATH.append(temp_bin_directory)
    env.PYTHONPATH.prepend("{root}/python")

    if system.platform == "linux":
        alias("deadline_minimonitor", "bash deadline_minimonitor.sh")
        alias("minimon", "bash deadline_minimonitor.sh")
