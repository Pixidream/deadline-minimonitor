# coding : utf-8
"""
------------------------------------------------------------------------------------------------------------
script : mini_uimw.py
usage : This Script start or kill slave
production destionation:  Used by artist to start or stop slave on there machine
------------------------------------------------------------------------------------------------------------
License info : All right reserved to circus. Please do not copy or redistribute without permissions
------------------------------------------------------------------------------------------------------------
Author : Francois Lavigne Marbach
Copyright : 2019 - Circus - MiniMonitor
Credits : Francois Lavigne Marbach | Arnaud Pasani | Adrien Meynard | Olivier Gilquin
License : Copyrighted
Version : 1.0
Maintainer : Francois Lavigne Marbach
Email : francois@circus.fr
Status : In Test
------------------------------------------------------------------------------------------------------------
TODO : init UI : Refactor UI avec Layout (later)
TODO : Tray Menu dans une methode
TODO : Add icon path in __init__
------------------------------------------------------------------------------------------------------------
"""

# -------------------------------------------------------------------------
# -------------------------------- Imports --------------------------------

# Build in
import time
import subprocess
import os

# Third-Party
from Qt import QtWidgets
from Qt.QtWidgets import QSystemTrayIcon, QAction, QApplication, QMenu, QLabel, QSlider, QMainWindow, QWidget, QStackedWidget, QFrame, QPushButton
from Qt.QtGui import QIcon, QFont, QPixmap
from Qt.QtCore import Signal, QObject, QThread, QSize, Qt, QRect, QCoreApplication, QMetaObject
import psutil

# -------------------------------------------------------------------------
# -------------------------------- Globals --------------------------------

__version__ = "0.1.0"


# -------------------------------------------------------------------------
# --------------------------------- Class --------------------------------


class WorkerObject(QThread):
	"""
	Create Thread to check Process every 10 second
	"""

	# create signal who return bool
	signalStatus = Signal(bool)

	def __init__(self, parent=None):
		"""
		init the QThread
		:param parent:
		"""
		super(self.__class__, self).__init__(parent)

	# def the script run in the thread (see check_process function)

	def run(self):
		"""
		run the process def in check_process() and emit bool signal
		every 10 seconds the  check_process() look for 'deadlineslave' in running process.
		It will return True if process run and false if he's is not.
		"""
		while True:
			self.signalStatus.emit(check_process())
			# def timer to x second before loop again
			time.sleep(10)


class ButtonSlider(QSlider):
	"""
	This class iterate over QSlider
	create a full custom slide button
	"""
	def __init__(self, parent=None):
		super(ButtonSlider, self).__init__(parent)

		# set the Widget size and Geometry
		self.setGeometry(QRect(60, 30, 51, 41))
		# StyleSheet to custom the Slider using layer to change color depending of position
		self.setStyleSheet("QSlider::groove:horizontal{"
						   "background-color: #bdc3c7;"
						   "border: 0px solid #424242;"
						   "height: 18px;"
						   "border-radius: 8px;}"
						   "QSlider::handle:horizontal{"
						   "background-color: #7f8c8d;"
						   "border: 2px solid #7f8c8d;"
						   "width: 16px;"
						   "height: 20px;"
						   "line-height: 20px;"
						   "margin-top: -5px;"
						   "margin-bottom: -5px;"
						   "border-radius: 10px;}"
						   "QSlider::handle:horizontal:hover{"
						   "border-radius: 10px;}"
						   "QSlider::sub-page:Horizontal{"
						   "background-color: #27ae60;"
						   "border : 0px solid;"
						   "border-radius: 8px;}"
						   "QSlider::add-page:Horizontal{"
						   "background-color: #e74c3c;"
						   "border : 0px solid;"
						   "border-radius: 8px;}")
		# set Maximum slider Value to 1
		self.setMaximum(1)
		# set step to 1 like this we have only 2 position (0,1)
		self.setPageStep(1)
		self.setOrientation(Qt.Horizontal)
		self.setInvertedAppearance(False)
		self.setInvertedControls(False)
		# remove the slider tick
		self.setTickPosition(QSlider.NoTicks)
		self.setObjectName("horizontalSlider")


class LabelClickable(QLabel):
	"""
	overclass QLabel to create a clickable label for the custom Windows Frame
	"""

	# Create Signal into click var
	click = Signal()

	def mousePressEvent(self, event):
		"""
		this function get mouse click event return it in click signal and emit it
		:param event: QEvent
		:return: click
		"""
		self.click.emit()


class Ui_MainWindow(object):
	"""
	create Main Window UI Class with the SliderButton a Custom Windows Frame, a little button to open deadline Monitor
	there is also a Stacked Widget to show the slave status. A TrayIcon with a context menu who update status icon too
	"""

	def __init__(self, MainWindow):
		"""
		init the MainWindow, check class UI_MainWindow for more info
		:param MainWindow: QWidget
		:return: create the main UI and the TrayIcon
		"""
		self.MainWindow = MainWindow
		self.MainWindow.setObjectName("MainWindow")
		# The main window size
		self.MainWindow.resize(275, 100)
		# set a min and max to be sure that users can't resize it
		self.MainWindow.setMinimumSize(QSize(275, 120))
		self.MainWindow.setMaximumSize(QSize(275, 120))
		font = QFont()
		font.setFamily("OpenSymbol")
		font.setStyleStrategy(QFont.PreferAntialias)
		self.MainWindow.setFont(font)
		self.MainWindow.setWindowOpacity(1.0)
		self.MainWindow.setStyleSheet("background-color: rgb(52, 73, 94);")
		self.MainWindow.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
		self.centralwidget = QWidget(self.MainWindow)
		self.centralwidget.setObjectName("centralwidget")

		# create horizontal slider with class ButtonSlider and set it in the centralwidget.
		self.horizontalSlider = ButtonSlider(self.centralwidget)
		# set the slider value dynamically depending of check_process
		self.horizontalSlider.setValue(check_process())
		# connect valueChanged to the change_value function to start or kill the slave
		self.horizontalSlider.valueChanged.connect(self.change_value)

		# create 'enable | disable' QLabel and set it in central Widget to document users for button action
		self.label_2 = QLabel(self.centralwidget)
		self.label_2.setGeometry(QRect(130, 40, 81, 21))
		font = QFont()
		font.setFamily("Leelawadee")
		font.setBold(False)
		font.setWeight(50)
		self.label_2.setFont(font)
		self.label_2.setStyleSheet("color : rgb(236, 240, 241);")
		self.label_2.setObjectName("label_2")

		# create the button to start deadline monitor and set it in the central widget
		self.pushButton = QPushButton(self.centralwidget)
		self.pushButton.setGeometry(QRect(180, 90, 91, 20))
		font = QFont()
		font.setFamily("Leelawadee")
		self.pushButton.setFont(font)
		# design a little bit and create a little hover effect
		self.pushButton.setStyleSheet("QPushButton"
									  "{background-color: #ecf0f1;"
									  "color : #34495e;"
									  "border : 0px solid;"
									  "border-radius: 10px;}"
									  "QPushButton::hover{"
									  "background-color: #bdc3c7;"
									  "color : #34495e;"
									  "border : 0px solid;"
									  "border-radius: 6px;}")
		self.pushButton.setObjectName("pushButton")
		self.pushButton.clicked.connect(self.call_deadline_monitor)

		#  create stackedWidget to show an updatable icon for the process status
		self.stackedWidget = QStackedWidget(self.centralwidget)
		self.stackedWidget.setGeometry(QRect(60, 90, 41, 31))
		self.stackedWidget.setFrameShadow(QFrame.Plain)
		self.stackedWidget.setObjectName("stackedWidget")

		# create enable widget. This is a QLabel inside QWidget container with green check icon
		self.enable = QWidget()
		self.enable.setObjectName("enable")
		self.enable_picto = QLabel(self.enable)
		self.enable_picto.setGeometry(QRect(10, 0, 21, 21))
		self.enable_picto.setMaximumSize(QSize(16777215, 31))
		self.enable_picto.setText("")
		self.enable_picto.setPixmap(QPixmap("resources/polling_status_32x32__684513.png"))
		self.enable_picto.setScaledContents(True)
		self.enable_picto.setObjectName("enable_picto")
		self.stackedWidget.addWidget(self.enable)

		# create disable widget. This is a QLabel inside QWidget container with red cross icon
		self.disable = QWidget()
		self.disable.setObjectName("disable")
		self.disable_picto = QLabel(self.disable)
		self.disable_picto.setGeometry(QRect(10, 0, 21, 21))
		self.disable_picto.setMaximumSize(QSize(16777215, 31))
		self.disable_picto.setText("")
		self.disable_picto.setPixmap(QPixmap("resources/status_rejected_684182.png"))
		self.disable_picto.setScaledContents(True)
		self.disable_picto.setObjectName("disable_picto")
		self.stackedWidget.addWidget(self.disable)

		# set the default index to 1. This value will change depending of deadlineslave process status
		self.stackedWidget.setCurrentIndex(1)

		# create the status_textlabel to explain to user the QStackedWidget
		self.status_textlabel = QLabel(self.centralwidget)
		self.status_textlabel.setGeometry(QRect(10, 90, 47, 21))
		font = QFont()
		font.setFamily("OpenSymbol")
		font.setPointSize(10)
		self.status_textlabel.setFont(font)
		self.status_textlabel.setStyleSheet("color: rgb(236, 240, 241);")
		self.status_textlabel.setObjectName("status_textlabel")

		# create the minimized icon label with the LabelClickable class and connect it to hide event
		self.minimized_frameicon = LabelClickable(self.centralwidget)
		self.minimized_frameicon.setGeometry(QRect(250, 5, 20, 20))
		self.minimized_frameicon.setText("")
		self.minimized_frameicon.setPixmap(QPixmap("resources/icons8-minimize-window-96.png"))
		self.minimized_frameicon.setScaledContents(True)
		self.minimized_frameicon.setObjectName("minimized_frameicon")
		self.minimized_frameicon.click.connect(self.MainWindow.hide)

		# set the central widget def text with retranslateUI and connectSoltsByName
		self.MainWindow.setCentralWidget(self.centralwidget)
		self.retranslateUi(self.MainWindow)
		QMetaObject.connectSlotsByName(self.MainWindow)

		# Init QSystemTrayIcon and set default icon (will change depending of deadlineslave process status)
		self.tray_icon = QSystemTrayIcon(self.MainWindow)
		self.tray_icon.setIcon(QIcon('resources/polling_status_32x32__684513.png'))

		'''
			Define and add steps to work with the system tray icon
			show - show window  c
			hide - hide window
			exit - exit from application
			enable - start slave
			disable - stop slave
		'''

		#  create abstract user interface with icon wich gonna be insert on tray icon context menu
		show_action = QAction(QIcon('resources/icons8-maximize-window-96.png'), "Show", self.MainWindow)
		quit_action = QAction(QIcon('resources/icons8-close-window-96.png'), "Exit", self.MainWindow)
		hide_action = QAction(QIcon('resources/icons8-minimize-window-96.png'), "Hide", self.MainWindow)
		enable_action = QAction(QIcon('resources/icons8-ok-96.png'), "Enable", self.MainWindow)
		disable_action = QAction(QIcon('resources/icons8-sleep-96.png'), "Disable", self.MainWindow)

		# connect the previously created action to there function (name are explicit)
		show_action.triggered.connect(self.myshow)
		hide_action.triggered.connect(self.MainWindow.hide)
		quit_action.triggered.connect(QApplication.quit)
		enable_action.triggered.connect(self.call_deadlineslave)
		disable_action.triggered.connect(self.kill_deadlineslave)

		# create tray icon context menu and set his design
		tray_menu = QMenu()
		font = QFont()
		font.setFamily('OpenSymbol')
		tray_menu.setFont(font)
		tray_menu.setStyleSheet("QMenu {background-color: #34495e; font-family: 'OpenSymbol'; color: #ecf0f1;} QMenu::selected{Background-color: #2c3e50;}")
		tray_menu.addAction(enable_action)
		tray_menu.addAction(disable_action)
		tray_menu.addSeparator()
		tray_menu.addAction(show_action)
		tray_menu.addAction(hide_action)
		tray_menu.addAction(quit_action)

		#  add the menu to tray icon context menu, connect it to double_click function and call the tray icon
		self.tray_icon.setContextMenu(tray_menu)
		self.tray_icon.activated.connect(self.double_click)
		self.tray_icon.show()

		# start thread to check if deadlineslave run and connect the signalStatus and return it to update_status as boolean
		self.worker_thread = WorkerObject()
		self.worker_thread.signalStatus.connect(self.update_status)
		self.worker_thread.start()

# -------------------------------------------------------------------------
# ------------------------------- Methods ------------------------------
# ---------------------------------- UI -----------------------------------

	def retranslateUi(self, MainWindow):
		"""
		this method is generated by QtDesigner it set Windows title and QLabel text
		:param MainWindow: QObject
		:return: text for MainWindow title and QLabel
		"""
		_translate = QCoreApplication.translate
		MainWindow.setWindowTitle(_translate("MainWindow", "Deadline - Minimonitor"))
		self.label_2.setText(_translate("MainWindow", "Disable | Enable"))
		self.pushButton.setText(_translate("MainWindow", "Deadline Monitor"))
		self.status_textlabel.setText(_translate("MainWindow", "Status : "))

	def update_status(self, value):
		"""
		get boolean return by worker_thread to change tray_icon icon and stacked_Widget
		if the signal is True we gonna have green checked icon for tray_icon and an index of 0 for stacked_Widget
		inverse for else:
		:param value: bool
		:return: bool value, int index, png icon
		"""
		print(value)
		if value:
			self.tray_icon.setIcon(QIcon('resources/polling_status_32x32__684513.png'))
			self.stackedWidget.setCurrentIndex(0)

		else:
			self.tray_icon.setIcon(QIcon("resources/status_rejected_684182.png"))
			self.stackedWidget.setCurrentIndex(1)

	def hide_app(self):
		"""
		this method just connect the hide event to MainWindow
		:return: hide signal
		"""
		self.MainWindow.hide()

	def press_key_event(self, event):
		"""
		this method will be used by the ButtonSlider to change his value when keyboard arrow are pressed
		left and down disable when up and right enable
		:param event: QEvent
		:return: int value
		"""
		if event.key() == Qt.Key_Right:
			self.horizontalSlider.setValue(self.horizontalSlider.value() + 1)
		elif event.key() == Qt.Key_Left:
			self.horizontalSlider.setValue(self.horizontalSlider.value() - 1)
		else:
			QtWidgets.press_key_event(self, event)

	def change_value(self, value):
		"""
		connected to SliderButton(), set action depending of slider position
		if the value is 1 we start the slave if she's equal to 0 we stop it
		:param value: int
		:return: return QSlider Value
		"""
		if value == 1:
			self.call_deadlineslave()
		else:
			self.kill_deadlineslave()

	def call_deadline_monitor(self):
		"""
		connected to the deadline monitor PushButton to start deadlinemonitor.exe
		:return:
		"""
		self._subprocess_cmd(GetDeadlineMonitor())

	def myshow(self):
		"""
		calculate the tray_icon position + widget height to show MainWindow at screen bottom
		:return: return TrayIcon position to show windows over him
		"""
		rect = self.tray_icon.geometry()
		topleft = rect.topLeft()
		print(topleft.x(), topleft.y() - self.MainWindow.size().height())
		self.MainWindow.move(topleft.x(), topleft.y() - self.MainWindow.size().height())
		self.MainWindow.show()

	def double_click(self, reason):
		"""
		connect double click event to tray_icon to show/hide MainWindow when double clicked
		:param reason: ActivationReason
		:return: give the reason of the QTrayIcon Activation
		"""
		if reason == QSystemTrayIcon.DoubleClick:
			tmp = self.MainWindow.isHidden()
			if tmp:
				self.myshow()
			else:
				self.MainWindow.hide()

# ---------------------------------- Script -----------------------------------
	def _subprocess_cmd(self, command):
		"""
		create subprocess method to run multiple command with one function.
		:param command: string
		:return:
		"""
		subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)

	def call_deadlineslave(self):
		"""
		get the deadline bin path and the deadlineslave command from GetDeadlineCommand() and run the slave with nogui
		:return: run process deadlineslave
		"""
		self._subprocess_cmd(GetDeadlineCommand() + ' -nogui')

	def kill_deadlineslave(self):
		"""
		this method get the deadline bin path and the deadlineslave command from GetDeadlineCommand() and stop the slave
		:return: kill process deadline slave
		"""
		self._subprocess_cmd(GetDeadlineCommand() + ' -shutdown')


# ---------------------------------------------------------------------------------------
# ------------------------------------ Functions --------------------------------------
def GetDeadlineCommand():
	"""
	:return: set deadline local path and return deadlineslave command
	"""
	deadlineBin = ""
	try:
		deadlineBin = os.environ['DEADLINE_PATH']
	except KeyError:
		# if the error is a key error it means that DEADLINE_PATH is not set. however Deadline command may be in the PATH or on OSX it could be in the file /Users/Shared/Thinkbox/DEADLINE_PATH
		pass

	# On OSX, we look for the DEADLINE_PATH file if the environment variable does not exist.
	if deadlineBin == "" and os.path.exists("/Users/Shared/Thinkbox/DEADLINE_PATH"):
		with open("/Users/Shared/Thinkbox/DEADLINE_PATH") as f:
			deadlineBin = f.read().strip()

	deadlineCommand = os.path.join(deadlineBin, "deadlineslave")

	return '"' + deadlineCommand + '"'


def GetDeadlineMonitor():
	"""
	:return: set deadline local path and return deadline monitor executable
	"""
	deadlineBin = ""
	try:
		deadlineBin = os.environ['DEADLINE_PATH']
	except KeyError:
		# if the error is a key error it means that DEADLINE_PATH is not set. however Deadline command may be in the PATH or on OSX it could be in the file /Users/Shared/Thinkbox/DEADLINE_PATH
		pass

	# On OSX, we look for the DEADLINE_PATH file if the environment variable does not exist.
	if deadlineBin == "" and os.path.exists("/Users/Shared/Thinkbox/DEADLINE_PATH"):
		with open("/Users/Shared/Thinkbox/DEADLINE_PATH") as f:
			deadlineBin = f.read().strip()

	deadlineCommand = os.path.join(deadlineBin, "deadlinemonitor.exe")

	return '"' + deadlineCommand + '"'


def check_process():
	"""
	function run on a separated thread who check every 10 sec if the deadlineslave process is running
	it return boolean
	:return: return if the process is running or not with a boolean
	"""
	while True:
		slave_actif = False
		for proc in psutil.process_iter():
			if 'deadlineslave'.lower() in proc.name().lower():
				slave_actif = True
				break
		if slave_actif:
			return True
		else:
			return False


# -------------------------------------------------------------------------
# -------------------------------- Default --------------------------------


if __name__ == "__main__":
	import sys

	app = QApplication(sys.argv)
	MainWindow = QMainWindow()
	MainWindow.setWindowIcon(QIcon("resources/111circus_logocopie1_copie_X3c_icon.png"))
	ui = Ui_MainWindow(MainWindow)
	sys.exit(app.exec_())